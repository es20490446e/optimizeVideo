#! /bin/bash

echo

ls /usr/share/mime/video |
rev |
cut --delimiter='.' --fields=2- |
rev |
xargs printf 'video/%s;'

echo
echo
